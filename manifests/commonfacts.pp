# @summary
#   This class handles the configuration file's.
#
# @api private
#
class commonbase::commonfacts inherits commonbase {

  file { "${commonbase::commonfactspath}":
    ensure => directory,
  }

  file { "${commonbase::commonfactspath}/facts.d":
    ensure => link,
    target => '/opt/puppetlabs/facter/facts.d',
  }

}
