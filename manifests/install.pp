# A description of what this class does
#
# @summary Prepare the base system
#
# @example
#   include commonbase::install
class commonbase::install inherits commonbase {

  $commonbase::commonbase_packages.each |String $package|{
    package { $package:
      ensure => installed,
    }
  }
}
