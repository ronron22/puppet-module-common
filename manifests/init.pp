# commonbase
#
# @summary Init class for commonbase module, includes other classes
#
# @param commonfactspath
#   The common facts path name's. Default value: undef.
#
class commonbase (

  Optional[String]  $commonfactspath = undef,
  Optional[Array]  $commonbase_packages = undef,

  ) {

  include commonbase::install
  include commonbase::commonfacts

}
